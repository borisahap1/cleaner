<?php

namespace app\controllers;

use Yii;
use app\models\Contact;
use yii\web\Controller;

class SiteController extends Controller
{
	
	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex()
	{
		$model = new Contact;
		
		return $this->render('index', [
			'model' => $model,
		]);
	}
	
	public function actionError()
	{
		return $this->redirect(['index']);
	}
	
	public function actionSendEmail()
	{
		$contact = new Contact();
		$contact->load(\Yii::$app->request->post());
		return Yii::$app->mailer->compose('@app/web/mail/new', ['contact' => $contact])
		                 ->setTo('expresscleanpro2020@gmail.com')
		                 ->setBcc('brandonmaxwelltwo@gmail.com')
		                 //->setFrom(['brandonmaxwelltwo@gmail.com' => 'Express Clean Pro'])
		                 ->setFrom(['info@express-clean.pro' => 'Express Clean Pro'])
		                 ->setSubject('Новая заявка')
		                 ->send();
	}
}
