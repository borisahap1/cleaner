<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap4\BootstrapPluginAsset;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AppAsset extends AssetBundle
{
	
	public $basePath = '@webroot';
	
	public $baseUrl  = '@web';
	
	public $css      = [
		//'css/bootstrap.css',
		'css/flexslider.css',
		'css/fontawesome-all.css',
		'css/ken-burns.css',
		'css/slider.css',
		'css/style.css',
		'css/swipebox.css',
	];
	
	public $js       = [
		'js/easing.js',
		'js/main.js',
		'js/move-top.js',
		'js/snap.svg-min.js',
	];
	
	public $depends  = [
		'yii\web\YiiAsset',
		BootstrapPluginAsset::class,
		JqueryAsset::class,
	];
}
