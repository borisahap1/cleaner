<?php
/**
 *
 * @author    Paul Stolyarov <teajeraker@gmail.com>
 * @copyright industrialax.com
 * @license   https://industrialax.com/crm-general-license
 */

namespace app\models;

use yii\base\Model;

class Contact extends Model
{
	
	public $name;
	
	public $email;
	
	public $phone;
	
	public function rules()
	{
		return [
			[['name', 'phone'], 'required','message' => 'Имя и телефон не могут быть пустыми полями'],
			[['email'], 'email','message' => 'Неправильный формат'],
		];
	}
	
}
