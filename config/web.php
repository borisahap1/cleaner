<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
	'id'         => 'basic',
	'basePath'   => dirname(__DIR__),
	'bootstrap'  => ['log'],
	'aliases'    => [
		'@bower' => '@vendor/bower-asset',
		'@npm'   => '@vendor/npm-asset',
	],
	'components' => [
		'request'      => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'Z5XwXsrfiIsm9CNmDO5DP_hlrVDRDayB',
			'baseUrl'             => '',
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'identityClass'   => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'assetManager'         => [
			'linkAssets' => true,
			'class'      => 'yii\web\AssetManager',
			'bundles'    => [
				'yii\web\JqueryAsset'                 => ['js' => ['jquery.min.js']],
				'yii\bootstrap4\BootstrapAsset'       => ['css' => ['css/bootstrap.min.css']],
				'yii\bootstrap4\BootstrapPluginAsset' => ['js' => ['js/bootstrap.bundle.min.js']],
				'extead\autonumeric\AutoNumericAsset' => [
					'depends' => [
						'yii\web\JqueryAsset',
						'yii\web\YiiAsset',
						'yii\bootstrap4\BootstrapAsset',
					],
				],
			],
		
		],
		'mailer'               => [
			'class'            => 'yii\swiftmailer\Mailer',
			'viewPath'         => '@app/web/mail',
			'useFileTransport' => false,
			//'transport'        => [
			//	'class'      => 'Swift_SmtpTransport',
			//	'host'       => 'smtp.gmail.com',
			//	'username'   => 'expresscleanpro2020@gmail.com',
			//	'password'   => 'Mikhalap13091989',
			//	'port'       => '465',
			//	'encryption' => 'ssl',
			//],
            'transport'        => [
				'class'      => 'Swift_SmtpTransport',
				'host'       => 'mailbe05.hoster.by',
				'username'   => 'info@express-clean.pro',
				'password'   => 'i>f3Aj&KD3',
				'port'       => '465',
				'encryption' => 'ssl',
			],
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db'           => $db,
		
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName'  => false,
			'rules'           => [
				'/' => 'site/index',
			],
		],
	
	],
	'params'     => $params,
];

if(YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
	
	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
}

return $config;
