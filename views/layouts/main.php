<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
<title>Химчистка мебели и ковровых покрытий в Минске</title>
	<?php $this->registerCsrfMetaTags() ?>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
    <meta name="yandex-verification" content="f9c624aa4965dad1" />
	<link rel="icon" type="image/png" sizes="32x32" href="images/favicon.png">
	<meta name="description"
	      content="Качественная быстрая химчистка мягкой мебели, диванов и ковровых покрытий в Минске. Выполним выездные услуги по чистке мебели на дому, офисе, кафе, ресторане. Выполним химчистку
	      салона авто">
	<meta name="keywords"
	      content="химчистка в Минске, все виды клининговых работ в Минске, клининг в Минске, клининговые услуги, химчистка мебели в Минске, химчистка ковровых покрытий, химчиста на дому в Минске, чистый диван, чистый стул, чистый ковер, химчистка стульев, химчистка дешево, химчистка быстро, химчистка дорого, химчистка профессионально, химчистка дешево, химчистка качественно, качественная химчистка, дорогая химчистка, дешевая химчистка, химчистка дома, удаление пятен и выведение запахов с двойной гарантией, химчистка в офисе, химчистка в кафе,химчистка в ресторане,химчистка на даче,химчистка в кинотеатре, химчистка салона авто,
авто химчистка в Минске, в Минске"/>
	
	<meta property="og:url"                content="http://express-clean.pro/" />
<meta property="og:type"               content="product" />
<meta property="og:title"              content="Химчистка мебели и ковровых покрытий в Минске" />
<meta property="og:description"        content="Качественная быстрая химчистка мягкой мебели, диванов и ковровых покрытий в Минске. Выполним выездные услуги по чистке мебели на дому, офисе, кафе, ресторане. Выполним химчистку
	      салона авто" />
<meta property="og:image"              content="http://express-clean.pro/images/og.jpeg" />
	
	<script type="application/x-javascript">
		addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
	</script>
	<!--// Meta tag Keywords -->

	<link href="css/slider.css" type="text/css" rel="stylesheet" media="all">

	<!-- css files -->
	<link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all"/> <!-- Style-CSS -->
	<link rel="stylesheet" href="css/fontawesome-all.css"> <!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->

	<!-- testimonials css -->
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property=""/><!-- flexslider css -->
	<!-- //testimonials css -->

	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Poiret+One&amp;subset=cyrillic,latin-ext" rel="stylesheet">
	<!-- //web-fonts -->
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="banner" id="home">
		<div class="cd-radial-slider-wrapper">

<!--Header-->
<header>
	<div class="container agile-banner_nav">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">

			<h1><img class="logo" src="images/logo.png" alt=""></h1>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
			        aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="#about">Главная</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#services">Наши улуги</a>
					</li>
					<!--					<li class="dropdown nav-item">-->
					<!--						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Pages-->
					<!--							<b class="caret"></b>-->
					<!--						</a>-->
					<!--						<ul class="dropdown-menu agile_short_dropdown">-->
					<!--							<li>-->
					<!--								<a href="error.html">Error Page</a>-->
					<!--							</li>-->
					<!--							<li>-->
					<!--								<a href="single.html">Single Page</a>-->
					<!--							</li>-->
					<!--						</ul>-->
					<!--					</li>-->
					<li class="nav-item">
						<a class="nav-link" href="#projects">Наши работы</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#contact">Оставить заявку</a>
					</li>
				</ul>
			</div>

		</nav>
	</div>
</header>
<!--Header-->

			<ul class="cd-radial-slider" data-radius1="60" data-radius2="1364" data-centerx1="110" data-centerx2="1290">
				<li class="visible">
					<div class="svg-wrapper">
						<svg viewBox="0 0 1400 800">
							<title>Animated SVG</title>
							<defs>
								<clipPath id="cd-image-1">
									<circle id="cd-circle-1" cx="110" cy="400" r="1364"/>
								</clipPath>
							</defs>
							<image height='800px' width="1400px" clip-path="url(#cd-image-1)" xlink:href="images/4.jpg"></image>
						</svg>
					</div> <!-- .svg-wrapper -->
					<div class="cd-radial-slider-content">
						<div class="wrapper">
							<div class="text-center">
								<h2>Химчистка мебели и </h2>
								<h3> ковровых покрытий</h3>
								<a href="#services" class="read"><strong>Наши услуги</strong></a>
								<a href="#contact" class="read"><strong>Оставить заявку</strong></a>
							</div>
						</div>
					</div> <!-- .cd-radial-slider-content -->
				</li>
			</ul> <!-- .cd-radial-slider -->
		</div> <!-- .cd-radial-slider-wrapper -->
	</div>
<?= $content ?>


<footer class="py-5">
	<div class="container">
		<div class="footer-logo mb-5 text-center">
			<img style="width: 27%" src="images/logo.png" alt="">
		</div>
		<div class="footer-grid">
			<div class="list-footer">
				<ul class="footer-nav text-center">
					<li>
						<a href="#about">Главная</a>
					</li>
					<li>
						<a href="#services">Наши услуги</a>
					</li>
					<li>
						<a href="#projects">Портфолио</a>
					</li>
					<li>
						<a href="#contact">Оставить заявку</a>
					</li>
				</ul>
			</div>
			<div class="agileits_w3layouts-copyright mt-4 text-center">
				<p>© 2020. Все права защищены</p>
		</div>
		</div>
	</div>
</footer>

	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap -->

	<script src="js/snap.svg-min.js"></script>
	<script src="js/main.js"></script>
	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		$(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                start: function (slider) {
                    $('body').removeClass('loading');
                }
            });
        });
	</script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
