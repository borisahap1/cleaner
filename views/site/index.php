<?php

use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>
<!-- about -->
<section class="wthree-row py-5">
    <div class="container py-lg-5 py-3">
        <a name="about"></a>
        <h3 class="heading mb-sm-5 mb-4"> Зачем нужна чистка мягкой мебели и ковровых покрытий? </h3>
        <p class="mt-4">
            По рекомендациям наших специалистов, чистку мягкой мебели и ковровых покрытий следует производить не реже, чем раз в полгода. Только в этом случае вы сможете не только поддерживать видимую
            чистоту изделий, но и продлить срок их службы, соблюдая гигиену.
            Впрочем, необходимая периодичность также будет зависеть от типа материала. Для ковров с длинным ворсом чистку рекомендуется проводить максимально часто (до 4 раз в год), в отличие от
            короткого. Для тканей с натуральным составом (шерсть, шелк) рекомендуется профессиональная чистка с применением специальных средств, которые помогут бережно устранить самые глубокие
            загрязнения.
            Дополнительным фактором при определении периодичности чистки служит наличие в доме детей и домашних животных, которые служат дополнительным источником загрязнения. Кроме того, шерсть
            животных может скапливаться в длинном ворсе и труднодоступных участках мебели. Стоит также принимать во внимание, что для безопасности ребенка чистка должна быть максимально частой – с
            целью профилактики возможных инфекций.
            Ворс ковров и обивка мягкой мебели имеют свойство впитывать до 70% пыли и до 30% других загрязнений, удерживая их внутри и представляя собой идеальную среду для размножения болезнетворных
            бактерий. Только профессиональный клининг гарантирует вам максимально глубокую чистку ковров и мягкой мебели с индивидуальным подбором средств для каждого типа материала и загрязнения. Это
            позволяет устранить загрязнения, скапливающиеся во внутренних слоях на протяжении долгого времени и представляющие опасность для вашего здоровья. Кроме того, профессиональная чистка не
            причиняет вреда ни людям, ни животным, ни изделиям, даже если они изготовлены из очень деликатных натуральных материалов.
            Специально подобранные средства и мощное оборудование позволяют избавиться не только от всех видов загрязнений, но и микроорганизмов, неприятного запаха. Также они помогают вернуть
            покрытию первоначальную яркость и мягкость.
            Можно сказать, что мягкая мебель и ковровые покрытия впитывают в себя пыль и грязь подобно губке. При этом известно, что пыль способна вызывать тяжелые аллергические реакции, и опасна для
            людей, которые страдают от заболеваний дыхательных путей. Поэтому важно позаботиться о том, чтобы очистить мебель от следов пыли. Пылесоса для этой задачи недостаточно: полностью удалить
            загрязнения, пылевых клещей и неприятные запахи, которыми пропиталась обивка, поможет только химчистка мягкой мебели.
            Химчистка мягкой мебели на дому позволяет добиться следующих целей:
        <p class="mt-2">
            - удалить застарелые пятна (кофе, косметика, жир, краска и т. д.);
        <p class="mt-1">
            - избавиться от неприятных запахов;
        <p class="mt-1">
            - вернуть мягкой мебели прежнюю фактуру за счет поднятия ворса;
        <p class="mt-1">
            - вернуть изделиям яркий, свежий вид.
        <p class="mt-3">
            Во время химчистки мягкой мебели на дому в Минске мы используем только современные средства, экологически чистые и безопасные для здоровья.
            Мы проводим химчистку мебели и ковров в 8 этапов.
        <p class="mt-4"></p>
        <p class="mt-4">
        <h3 class="heading text-capitalize mb-sm-5 mb-4"> Этапы чистки:</h3>
        <div class="row service-grids mt-5">
            <div class="col-lg-4 col-md-6 service-grid1">
                <h3>Осмотр</h3>
                <div class="row">
                    <div class="col-md-3 col-2">
                        <h4>1</h4>
                    </div>
                    <div class="col-md-9 col-10">
                        <p>На данном этапе производится осмотр изделия: определяется тип ткани, характер загрязнений, подбор пятновыводителей.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-md-0 mt-5 service-grid1">
                <h3>Сухая чистка</h3>
                <div class="row">
                    <div class="col-md-3 col-2">
                        <h4>2</h4>
                    </div>
                    <div class="col-md-9 col-10">
                        <p>Обивку пылесосят, чтобы удалить пыль и посторонние элементы.</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 mt-lg-0 mt-5 service-grid1">
                <h3>Выведение пятен</h3>
                <div class="row">
                    <div class="col-md-3 col-2">
                        <h4>3</h4>
                    </div>
                    <div class="col-md-9 col-10">
                        <p>Выводим локальные пятна краски, ручки, крови и др.</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 mt-5 service-grid1">
                <h3>Замачивание</h3>
                <div class="row">
                    <div class="col-md-3 col-2">
                        <h4>4</h4>
                    </div>
                    <div class="col-md-9 col-10">
                        <p>Поверхность мебели обрабатывается специально подобранными чистящими средствами с антибактериальным действием.</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 mt-5 service-grid1">
                <h3>Локальная обработка</h3>
                <div class="row">
                    <div class="col-md-3 col-2">
                        <h4>5</h4>
                    </div>
                    <div class="col-md-9 col-10">
                        <p>Сильно загрязненные места обрабатываются щетками определенной жесткости.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-5 service-grid1">
                <h3>Экстракторная чистка</h3>
                <div class="row">
                    <div class="col-md-3 col-2">
                        <h4>6</h4>
                    </div>
                    <div class="col-md-9 col-10">
                        <p>Глубинная промывка обработанной поверхности экстракторным оборудованием.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-5 service-grid1">
                <h3>Нейтрализация</h3>
                <div class="row">
                    <div class="col-md-3 col-2">
                        <h4>7</h4>
                    </div>
                    <div class="col-md-9 col-10">
                        <p>Специальным ополаскивателем производится нейтрализация остатков химии.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-5 service-grid1">
                <h3>Сушка</h3>
                <div class="row">
                    <div class="col-md-3 col-2">
                        <h4>8</h4>
                    </div>
                    <div class="col-md-9 col-10">
                        <p>Удаляется остаточная влага, производится расчесывание ворса.</p>
                    </div>
                </div>
            </div>
            <!--			<div class="col-lg-4 col-md-6 mt-5 service-grid1">-->
            <!--				<h3>Office Furniture</h3>-->
            <!--				<div class="row">-->
            <!--					<div class="col-md-3 col-2">-->
            <!--						<h4>9</h4>-->
            <!--					</div>-->
            <!--					<div class="col-md-9 col-10">-->
            <!--						<p>Phasellus iaculis sapien in tellus gravida, a placerat lacus elementum. Nulla vitae lacus nec elit mollis pretium.</p>-->
            <!--					</div>-->
            <!--				</div>-->
            <!--			</div>-->
        </div>
    </div>
</section><!-- //about -->

<!-- why choose us -->
<section class="why">
    <div class="layer py-5">
        <a name="services"></a>
        <div class="container py-3">
            <h3 class="heading text-capitalize mb-sm-5 mb-4">Наши услуги</h3>
            <div class="row why-grids">
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/sofa-7.svg" alt="">
                    <div class="text-center">
                        <h4>Кресло</h4>
                        <div class="agile-dish-caption"><h4>От 20 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/1.svg" alt="">
                    <div class="text-center">
                        <h4>Диван 2-х местный</h4>
                        <div class="agile-dish-caption"><h4>От 35 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/sofa-3-size.svg" alt="">
                    <div class="text-center">
                        <h4>Диван 3-х местный</h4>
                        <div class="agile-dish-caption"><h4>От 40 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/corner-sofa.svg" alt="">
                    <div class="text-center">
                        <h4>Угловой диван 3-х местный</h4>
                        <div class="agile-dish-caption"><h4>От 45 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/sofa-4-size.svg" alt="">
                    <div class="text-center">
                        <h4>Диван 4-х местный</h4>
                        <div class="agile-dish-caption"><h4>От 45 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/sofa-10.svg" alt="">
                    <div class="text-center">
                        <h4>Мягкий уголок с креслами</h4>
                        <div class="agile-dish-caption"><h4>От 65 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/sofa-6.svg" alt="">
                    <div class="text-center">
                        <h4>Диван-кровать</h4>
                        <div class="agile-dish-caption"><h4>От 40 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/matrac.svg" alt="">
                    <div class="text-center">
                        <h4>Матрас 160х200 (2стороны)</h4>
                        <div class="agile-dish-caption"><h4>От 40 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1">
                    <img style="width: 250px;height: auto" src="images/sofa9.svg" alt="">
                    <div class="text-center">
                        <h4>Стул(без спинки)</h4>
                        <div class="agile-dish-caption"><h4>От 4 BYN</h4></div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 why-grid1 mt-5">
                    <img style="width: 230px;height: auto" src="images/cover.svg" alt="">
                    <div class="text-center" style="margin-top: 32px">
                        <h4>Ковры и ковровые покрытия</h4>
                        <div class="agile-dish-caption"><h4>От 4 BYN/м.кв.</h4></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- //why choose us --><!--Gallery-->
<div class="gallery py-5">
    <div class="container py-sm-3">
        <a name="projects"></a>
        <h2 class="heading text-capitalize mb-sm-5 mb-3"> Наши работы </h2>
        <div class="row gallery-grids">
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInLeft animated" data-wow-delay=".5s">
                <!--							<a href="images/foto2_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto2_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Двух местный диван</h4>
                            <span class="separator"></span>
                            <p>Глубокая чистка, выведением пятен.</p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInLeft animated" data-wow-delay=".5s">
                <!--							<a href="images/foto3_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto3_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Ковер 2.2х1.4</h4>
                            <span class="separator"></span>
                            <p>Чистка ковра после 5-ти лет использования</p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInLeft animated" data-wow-delay=".5s">
                <!--							<a href="images/foto1_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto1_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Стул со спинкой</h4>
                            <span class="separator"></span>
                            <p>3 этапная чистка</p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInUp animated" data-wow-delay=".5s">
                <!--							<a href="images/foto7_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto7_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Диван</h4>
                            <span class="separator"></span>
                            <p>Гостиный двухместный диван.</p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small ban-mar wow fadeInUp animated" data-wow-delay=".5s">
                <!--							<a href="images/foto4_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto4_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Матрас 160х200</h4>
                            <span class="separator"></span>
                            <p>Двух стороняя химчитска,с двойным устранением запоха</p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small ban-mar wow fadeInUp animated" data-wow-delay=".5s">
                <!--							<a href="images/foto8_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto8_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Стулья без спинки</h4>
                            <span class="separator"></span>
                            <p>Кухоные стулья без спинки</p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three wow ban-mar fadeInLeft animated gap-w3" data-wow-delay=".5s">
                <!--							<a href="images/foto5_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto5_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Матрас 180х200</h4>
                            <span class="separator"></span>
                            <p></p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three wow ban-mar fadeInLeft animated" data-wow-delay=".5s">
                <!--							<a href="images/foto9_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto9_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Кресло </h4>
                            <span class="separator"></span>
                            <p></p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three thre ban-mar wow fadeInLeft animated" data-wow-delay=".5s">
                <!--							<a href="images/foto11_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto11_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Димван-кровать</h4>
                            <span class="separator"></span>
                            <p>Глубокая чистка</p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three thre ban-mar wow fadeInLeft animated" data-wow-delay=".5s">
                <!--							<a href="images/foto12_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto12_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>Тахта</h4>
                            <span class="separator"></span>
                            <p>Тахта без спинок</p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three thre ban-mar wow fadeInLeft animated" data-wow-delay=".5s">
                <!--							<a href="images/foto13_edited.jpg" class="b-link-stripe b-animate-go  swipebox">-->
                <div class="gal-spin-effect vertical ">
                    <img src="images/foto13_edited.jpg" alt=" "/>
                    <div class="gal-text-box">
                        <div class="info-gal-con">
                            <h4>3-ех местный угловой диван</h4>
                            <span class="separator"></span>
                            <p></p>
                            <span class="separator"></span>

                        </div>
                    </div>
                </div>
                <!--							</a>-->
            </div>
        </div>
    </div>
</div><!-- //gallery --><!--&lt;!&ndash; team &ndash;&gt;--><!--<section class="w3ls-team py-5">--><!--	<div class="container py-3">--><!--		<h3 class="heading text-capitalize mb-sm-5 mb-4"> Our Team </h3>--><!--		<div class="row team-grids">--><!--			<div class="col-md-3 col-sm-6 w3_agileits-team1">--><!--				<img class="img-fluid" src="images/t1.jpg" alt="">--><!--				<h5 class="mt-3">Elmore</h5>--><!--				<p>Lorem ipsum</p>--><!--				<div class="social-icons mt-2">--><!--					<ul>--><!--						<li>--><!--							<a href="#" class="fab fa-facebook-f icon-border facebook"> </a>--><!--						</li>--><!--						<li class="mx-1">--><!--							<a href="#" class="fab fa-twitter  icon-border twitter"> </a>--><!--						</li>--><!--						<li>--><!--							<a href="#" class="fab fa-google-plus-g icon-border googleplus"> </a>--><!--						</li>--><!--					</ul>--><!--				</div>--><!--			</div>--><!--			<div class="col-md-3 col-sm-6 mt-sm-0 mt-5 w3_agileits-team1">--><!--				<img class="img-fluid" src="images/t2.jpg" alt="">--><!--				<h5 class="mt-3">Blanton</h5>--><!--				<p>Lorem ipsum</p>--><!--				<div class="social-icons mt-2">--><!--					<ul>--><!--						<li>--><!--							<a href="#" class="fab fa-facebook-f icon-border facebook"> </a>--><!--						</li>--><!--						<li class="mx-1">--><!--							<a href="#" class="fab fa-twitter  icon-border twitter"> </a>--><!--						</li>--><!--						<li>--><!--							<a href="#" class="fab fa-google-plus-g icon-border googleplus"> </a>--><!--						</li>--><!--					</ul>--><!--				</div>--><!--			</div>--><!--			<div class="col-md-3 col-sm-6 mt-md-0 mt-5 w3_agileits-team1">--><!--				<img class="img-fluid" src="images/t3.jpg" alt="">--><!--				<h5 class="mt-3"> Bass</h5>--><!--				<p>Lorem ipsum</p>--><!--				<div class="social-icons mt-2">--><!--					<ul>--><!--						<li>--><!--							<a href="#" class="fab fa-facebook-f icon-border facebook"> </a>--><!--						</li>--><!--						<li class="mx-1">--><!--							<a href="#" class="fab fa-twitter  icon-border twitter"> </a>--><!--						</li>--><!--						<li>--><!--							<a href="#" class="fab fa-google-plus-g icon-border googleplus"> </a>--><!--						</li>--><!--					</ul>--><!--				</div>--><!--			</div>--><!--			<div class="col-md-3 col-sm-6 mt-md-0 mt-5 w3_agileits-team1">--><!--				<img class="img-fluid" src="images/t4.jpg" alt="">--><!--				<h5 class="mt-3"> Glickon</h5>--><!--				<p>Lorem ipsum</p>--><!--				<div class="social-icons mt-2">--><!--					<ul>--><!--						<li>--><!--							<a href="#" class="fab fa-facebook-f icon-border facebook"> </a>--><!--						</li>--><!--						<li class="mx-1">--><!--							<a href="#" class="fab fa-twitter  icon-border twitter"> </a>--><!--						</li>--><!--						<li>--><!--							<a href="#" class="fab fa-google-plus-g icon-border googleplus"> </a>--><!--						</li>--><!--					</ul>--><!--				</div>--><!--			</div>--><!--		</div>--><!--	</div>--><!--</section>--><!--&lt;!&ndash; //team &ndash;&gt;-->

<!-- contact -->
<section class="contact py-5">
    <div class="container">
        <a name="contact"></a>
        <h2 class="heading text-capitalize mb-sm-5 mb-4"> Заказать звонок </h2>
        <div class="mail_grid_w3l">


            <div class="row">
                <div class="col-md-6 contact_left_grid" data-aos="fade-right">
                    <div class="alert alert-success alert-dismissible fade show js-alert" style="display: none" role="alert">
                        <strong>Запрос отправлен!</strong> В ближайшее время мы с Вами свяжемся
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <?php $form = ActiveForm::begin(['id' => 'js-form']) ?>
                    <div class="js-form">
                        <div class="contact-fields-w3ls">
                            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
                        </div>
                        <div class="contact-fields-w3ls">
                            <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
                        </div>
                        <div class="contact-fields-w3ls">
                            <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон'])->widget(MaskedInput::class, [
                                'mask'    => '+375 (9{2}) 9{3}-9{2}-9{2}',
                                'options' =>
                                    [
                                        'placeholder' => '+375 (29) 123-12-34',
                                    
                                    ],
                            ])->label(false) ?>
                        </div>
                        <button class="submit" type="submit">Отправить</button>
                    </div>
                    <?php ActiveForm::end() ?>

                </div>
                <div class="col-md-6 contact_left_grid" data-aos="fade-left">
                    <div class="contact-info p-sm-5 p-4">
                        <div class="mb-5">
                            <h4 class="mb-3">Контактная информация</h4>
                            <p><span class="fas fa-phone mr-2"></span><a href="tel:+375445699799">+375 44 5699799 (MTC)</a></p>
                            <p><span class="fas fa-envelope mr-2"></span> <a href="mailto:name@example.com">expresscleanpro2020@gmail.com</a></p>
                            <p><span class="fas fa-map-marker mr-2"></span> r.Минск, Минский район(по-договоренности) </p>
                            <div class="social my-3  text-center">
                                <ul class="d-flex justify-content-center">
                                    <!--					<li class="mx-2"><a href="#"><span class="fab fa-vk"></span></a></li>-->
                                    <li class="mx-2"><a target="_blank" href="https://www.instagram.com/express_clean_pro"><span class="fab fa-instagram"></span></a></li>
                                    <!--					<li class="mx-2"><a href="#"><span class="fab fa-viber"></span></a></li>-->
                                </ul>
                            </div>
                        </div>
                        <div class="">
                            <h4 class="mb-3">Время работы</h4>
                            <p><span class="fas fa-clock mr-2"></span> 24/7, без выходных и праздничных дней.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php $js = <<< JS
	$(document).on('submit','#js-form',function() {
	  let form = $(this)
	  $('.js-alert').css('display','block')
	  $.post({
	  url:'/site/send-email',
	  data:form.serialize(),
	  success:function() {
	  $('#contact-name').val('')
	  $('#contact-email').val('')
	  $('#contact-phone').val('')
	  }
	  })
	  return false
	})
JS;
$this->registerJs($js);
?>
